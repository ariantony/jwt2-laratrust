<?php

namespace App\Models;

use Laratrust\Models\LaratrustRole;

class Role extends LaratrustRole
{
    //public $incrementing = false;
    public $timestamps = false;

    protected $fillable = ['name','display_name','description'];

    protected $guarded = [];

    protected $hidden = [
        'created_at',
        'updated_at'

    ];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i:s',
        'updated_at' => 'datetime:Y-m-d H:i:s'
    ];
}
