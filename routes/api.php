<?php

use App\Http\Controllers\Author\PermissionRoleController;
use App\Http\Controllers\Author\RoleUserController;
use Illuminate\Support\Facades\Route;
use Laratrust\Http\Controllers\PermissionsController;

/*
    |--------------------------------------------------------------------------
    | API Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register API routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | is assigned the "api" middleware group. Enjoy building your API!
    |
*/

Route::group(
    [
        'middleware' => 'api',
        'namespace'  => 'App\Http\Controllers',
        'prefix'     => 'auth',
    ],
    function ($router) {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'AuthController@register');
        Route::post('logout', 'AuthController@logout');
        Route::get('profile', 'AuthController@profile');
        Route::post('refresh', 'AuthController@refresh');
    }
);

Route::group(
    [
        'middleware' => 'api',
        'namespace'  => 'App\Http\Controllers',
    ],
    function ($router) {
        Route::resource('todos', 'TodoController');
    }
);

Route::group(
    [
        'middleware' => 'api',
        'namespace'  => 'App\Http\Controllers',
        'prefix'     => 'privileges',
    ],
    function ($router) {
       Route::get('roles',[RoleUserController::class,'index']);
       Route::get('role/{id}',[RoleUserController::class,'show']);
       Route::get('role/search',[RoleUserController::class,'search']);
       Route::post('roles',[RoleUserController::class,'store']);
       Route::put('role/{id}',[RoleUserController::class,'update']);
       Route::delete('role/{id}',[RoleUserController::class,'destroy']);

       Route::get('permissions',[PermissionRoleController::class,'index']);
       Route::get('permission/{id}',[PermissionRoleController::class,'show']);
       Route::get('permission/search',[PermissionRoleController::class,'search']);
       Route::post('permissions',[PermissionRoleController::class,'store']);
       Route::put('permission/{id}',[PermissionRoleController::class,'update']);
       Route::delete('permission/{id}',[PermissionRoleController::class,'destroy']);
    }
);
